let items = []
document.getElementById("save").style.display = "none"
getTasks()

//render so that the array and the output on the website stays the same
function renderList(list) {
    const listElement = document.getElementById("list")
    listElement.innerText = ""

    for (let listItem of list) {

        const newItemElement = [document.createElement("li"), document.createElement("INPUT"), document.createElement("button")]

        newItemElement[2].innerText = "Delete"
        newItemElement[2].classList.add("deletebutton")

        newItemElement[0].innerText = listItem.title ?? listItem
        newItemElement[0].classList.add("submitedlist")
        newItemElement[1].classList.add("inputofedit")
        newItemElement[1].value = listItem.title ?? listItem
        document.getElementsByClassName("submitedlist")

        newItemElement[2].addEventListener('click', () => {
            items.splice(items.indexOf(listElement), 1)
            deleteTasks
            renderList(items)

        })

        listElement.append(newItemElement[0], newItemElement[1], newItemElement[2])
        newItemElement[1].style.display = "none"


    }
}

document.forms[0].addEventListener('submit', function (event) {
    //doesn't allow a page-reset on formula send
    event.preventDefault()

    const newItemInputElement = document.forms[0].newItemElement
    const itemText = newItemInputElement.value

    if (itemText == "") {
        alert("Die Aufgabe muss einen Text beinhalten!")
        return
    }

    fetch("http://localhost:3000/auth/cookie/tasks", {
        credentials:"include",
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ title: itemText })
    })
        .then(response => {
            if (response.ok) {
               getTasks()
            } else {
                alert("Failed")
            }
        })

    newItemInputElement.value = "";



})

document.getElementById("reset").addEventListener('click', function (event) {
    event.preventDefault();
    items = [];
    renderList(items)
})

document.getElementById("edit").addEventListener('click', function (event) {
    event.preventDefault();

    document.getElementById("save").style.display = "block"
    document.getElementById("submit").style.display = "none"
    Array.from(document.getElementsByClassName("submitedlist")).forEach(element => element.style.display = "none")
    Array.from(document.getElementsByClassName("inputofedit")).forEach(element => element.style.display = "block")

})


document.getElementById("editForm").addEventListener('submit', function (event) {
    event.preventDefault();
    let editedlist = [];
    Array.from(document.getElementsByClassName("inputofedit")).forEach(element => {
        if (element.value != '') {
            editedlist.push(element.value)
        }
    })
    document.getElementById("submit").style.display = "block"
    document.getElementById("save").style.display = "none"

    items = editedlist;
    renderList(items);
})

function getTasks() {

    fetch("http://localhost:3000/auth/cookie/tasks", { credentials: "include", method: "GET" }).then((r) => r.json())
        .then(json => {
            items = json
            renderList(items)

        })
}
function deleteTasks() {

    fetch("http://localhost:3000/auth/cookie/tasks", { credentials:"include", method: "DELETE" }).then((r) => r.json())
        .then(json => {
            items = json
            renderList(items)

        })
}




